#!/bin/bash
set -e

ps -p $$
# Allow secrets usage by USER_PASS_FILE variable
if [ "$USER_PASS_FILE" ] && [ -f $USER_PASS_FILE ]; then
    export USER_PASS=`cat $USER_PASS_FILE`
fi
if [ "$USER_PASS" ]; then
    echo geoserver:"$USER_PASS" |chpasswd
fi

chown -R 999:999 /mnt/geoserver_geodata &

# start openssh server
exec /usr/sbin/sshd -D
