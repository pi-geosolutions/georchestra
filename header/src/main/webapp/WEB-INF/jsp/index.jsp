<%--

 Copyright (C) 2009-2018 by the geOrchestra PSC

 This file is part of geOrchestra.

 geOrchestra is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option)
 any later version.

 geOrchestra is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 geOrchestra.  If not, see <http://www.gnu.org/licenses/>.

--%>

<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" %>
<%@ page import="java.util.*" %>
<%@ page import="org.georchestra._header.Utf8ResourceBundle" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page import="org.springframework.web.servlet.support.RequestContextUtils" %>
<%@ page import="org.georchestra.commons.configuration.GeorchestraConfiguration" %>
<%@ page import="java.util.regex.Matcher" %>
<%@ page import="java.util.regex.Pattern" %>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page isELIgnored="false" %>
<%
Boolean anonymous = true;

/*
response.setDateHeader("Expires", 31536000);
response.setHeader("Cache-Control", "private, max-age=31536000");
*/

// Using georchestra autoconf
String defaultLanguage = null;
String georLdapadminPublicContextPath = null;
String ldapadm = null;
try {
  ApplicationContext ctx = RequestContextUtils.getWebApplicationContext(request);
  defaultLanguage = ctx.getBean(GeorchestraConfiguration.class).getProperty("language");
  georLdapadminPublicContextPath = ctx.getBean(GeorchestraConfiguration.class).getProperty("consolePublicContextPath");
} catch (Exception e) {}

// to prevent problems with proxies, and for now:
response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
response.setHeader("Pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("Expires", 0); // Proxies.

String active = request.getParameter("active");
if (active == null) {
    active = "none";
}

if (georLdapadminPublicContextPath != null)
    ldapadm = georLdapadminPublicContextPath;
else
    ldapadm = "/console";

Locale rLocale = request.getLocale();

String detectedLanguage = rLocale.getLanguage();
String forcedLang = request.getParameter("lang");
String gnUrlLang = request.getParameter("gnlang");
String gnService = request.getParameter("gnservice");
String gnPage = request.getParameter("gnpage");

String lang = defaultLanguage;

if (forcedLang != null) {
    if (forcedLang.equals("en") || forcedLang.equals("es") || forcedLang.equals("ru") || forcedLang.equals("fr") || forcedLang.equals("de") || forcedLang.equals("sk")) {
        lang = forcedLang;
    }
} else {
    if(gnUrlLang!=null && !gnUrlLang.equals("")) {
        if(gnUrlLang.equals("slo")) {
            lang = "sk";
        } else {
            lang = gnUrlLang.substring(0, 2);
        }
    }
    else if (detectedLanguage.equals("en") || detectedLanguage.equals("es") || detectedLanguage.equals("ru") || detectedLanguage.equals("fr") || detectedLanguage.equals("de") || detectedLanguage.equals("sk")) {
        lang = detectedLanguage;
    }
}

String gnLang = lang;
ResourceBundle bundle = org.georchestra._header.Utf8ResourceBundle.getBundle("_header.i18n.index", new Locale(lang));

if(gnLang.equals("sk")) {
    gnLang = "slo";
}
if(gnLang.equals("en")) {
    gnLang = "eng";
}
if(gnLang.equals("fr")) {
    gnLang = "fre";
}

javax.servlet.jsp.jstl.core.Config.set(
    request,
    javax.servlet.jsp.jstl.core.Config.FMT_LOCALIZATION_CONTEXT,
    new javax.servlet.jsp.jstl.fmt.LocalizationContext(bundle)
);

Boolean extractor = false;
Boolean admin = false;
Boolean catadmin = false;
Boolean console = false;
Boolean analyticsadmin = false;
Boolean extractorappadmin = false;
String sec_roles = request.getHeader("sec-roles");
if(sec_roles != null) {
    String[] roles = sec_roles.split(";");
    for (int i = 0; i < roles.length; i++) {
        if (roles[i].equals("ROLE_GN_EDITOR") || roles[i].equals("ROLE_GN_REVIEWER") || roles[i].equals("ROLE_GN_ADMIN") || roles[i].equals("ROLE_ADMINISTRATOR") || roles[i].equals("ROLE_USER")) {
            anonymous = false;
        }
        if (roles[i].equals("ROLE_SUPERUSER")) {
            admin = true;
            console = true;
        }
        if (roles[i].equals("ROLE_ORGADMIN")) {
            admin = true;
            console = true;
        }
        if (roles[i].equals("ROLE_GN_ADMIN")) {
            admin = true;
            catadmin = true;
        }
        if (roles[i].equals("ROLE_ADMINISTRATOR")) {
            admin = true;
            extractorappadmin = true;
        }
    }
}

%>

<%!
    public final String switchLang(String lang, String active, String gnService, String gnPage) {
        String redirectURL = "";

        if(active.equals("geonetwork")) {
            if(lang.equals("sk")) lang = "slo";

            redirectURL = "/geonetwork/srv/" + lang +"/" + gnService;

            if(gnService != null && gnService.equals("catalog.search") && gnPage != null && !gnPage.equals("")) {
                redirectURL += "#/" + gnPage;
            }
        }
        else if(active.equals("mapfishapp")) {
            redirectURL = "/mapfishapp?lang=" + lang;
        }
        return redirectURL;
    }

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <base target="_parent" />
    <style type="text/css">
        /* see https://github.com/georchestra/georchestra/issues/147 for missing http protocol */
        @import url(//fonts.gstatic.com/s/raleway/v12/1Ptrg8zYS_SKggPNwN4rWqZPAA.woff2);

        html, body {
            padding     : 0;
            margin      : 0;
            background  : #fff;
        }
        #go_head {
            padding     : 0;
            margin      : 0.6em;
            font-family : Raleway,Arial,Helvetica,sans-serif;
            background  : #fff;
        }
        #go_home {
            float  : left;
        }
        #go_home img {
            border : none;
        }
        #go_head ul {
            float      : left;
            list-style : none;
            margin     : 7px 0px 0px 60px;
            padding    : 0;
            font-size  : 10pt;
            display    : inline;
        }
        #go_head li {
            right      : 100%;
            left       : 0;
            margin     : 0;
            padding    : 0;
            position   : relative;
            display    : inline-block;
            transition : right .3s ease, left .3s ease, background .3s ease;
            background : transparent;
        }
        #go_head li a {
            color              : #444;
            font-weight: 700;
            display            : inline-block;
            padding            : 0 0.3em;
            margin             : 0 0.3em;
            text-decoration    : none;
            transition         : background .3s ease-in;
            transition-property         : background,color,border-radius;
        }
        #go_head li a:first-letter {
            text-transform: uppercase;
        }
        #go_head li a:hover {
            color              : #f98182;
        }
        #go_head ul li.active a {
        }
        #go_head .logged {
        }
        #go_head .logged span{
            color: #000;
        }
        #go_head .logged span.light{
            color: #ddd;
        }
        #go_head .logged {
            position : relative;
        }
        #go_head .logged div {
            position : absolute;
            top      : 16px;
            right    : 10px;
        }
        #go_head .logged a {
            color : #444;
            font-weight: 700;
        }
        #go_head .logged a:first-letter {
            text-transform: uppercase;
        }
        #go_head ul ul {
            display: none;
        }
        #go_head li li {
            right: auto;
        }
        #go_head .expanded {
            position    : absolute;
            right       : 200px;
            left        : 250px;
            top         : 20px;
            background  : white;
            z-index     : 1;
            min-width   : 20em;
        }
        #go_head .expanded ul{
            display: block;
        }
        #go_head .expanded > a,
        #go_head .expanded ul{
            margin-top: 0;
            float: right;
        }
        #go_head .expanded > a {
            color: white;
            background: #666;
        }
        #go_head .group > a:after {
            content: ' »';
        }
        #go_head .expanded > a:before {
            content: '« ';
        }
        #go_head .expanded > a:after {
            content: '';
        }
        #lang_switcher {
            font-size: 10pt;
            float: right;
        }
    </style>

</head>


<body>

    <div id="go_head">
        <a href="/" id="go_home" title="<fmt:message key='go.home'/>">
            <img src="img/logo.png" alt="<fmt:message key='logo'/>"/>
        </a>
        <ul>
        <c:choose>
            <c:when test='<%= active.equals("geonetwork") %>'>
            <li class="active"><a href="/geonetwork/srv/<%= gnLang %>/catalog.search#/search"><fmt:message key="catalogue"/></a></li>
            </c:when>
            <c:otherwise>
            <li><a href="/geonetwork/srv/<%= gnLang %>/catalog.search#/search"><fmt:message key="catalogue"/></a></li>
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when test='<%= active.equals("mapfishapp") %>'>
            <li class="active"><a><fmt:message key="viewer"/></a></li>
            </c:when>
            <c:otherwise>
            <li><a href="/mapfishapp/"><fmt:message key="viewer"/></a></li>
            </c:otherwise>
        </c:choose>

            <c:choose>
                <c:when test='<%= admin == true %>'>
                    <c:choose>
                        <c:when test='<%= active.equals("geoserver") %>'>
                            <li class="active"><a href="/geoserver/web/"><fmt:message key="services"/></a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="/geoserver/web/"><fmt:message key="services"/></a></li>
                        </c:otherwise>
                    </c:choose>
                </c:when>
            </c:choose>

            <c:choose>
            <c:when test='<%= admin == true %>'>
            <li class="group"> 
                <a href="#admin"><fmt:message key="admin"/></a>
                <ul>

                    <c:choose>
                        <c:when test='<%= catadmin == true %>'>
                        <c:choose>
                            <c:when test='<%= active.equals("geonetwork") %>'>
                        <!-- GN2 or GN3 -->
                        <li class="active"><a href="/geonetwork/srv/<%= gnLang %>/admin.console"><fmt:message key="catalogue"/></a></li>
                            </c:when>
                            <c:otherwise>
                        <!-- GN2 or GN3 -->
                        <li><a href="/geonetwork/srv/<%= gnLang %>/admin.console"><fmt:message key="catalogue"/></a></li>
                            </c:otherwise>
                        </c:choose>
                        </c:when>
                    </c:choose>

                    <c:choose>
                        <c:when test='<%= extractorappadmin == true %>'>
                        <c:choose>
                            <c:when test='<%= active.equals("extractorappadmin") %>'>
                        <li class="active"><a href="/extractorapp/admin/"><fmt:message key="extractor"/></a></li>
                            </c:when>
                            <c:otherwise>
                        <li><a href="/extractorapp/admin/"><fmt:message key="extractor"/></a></li>
                            </c:otherwise>
                        </c:choose>
                        </c:when>
                    </c:choose>

                    <c:choose>
                        <c:when test='<%= console == true %>'>
                        <c:choose>
                            <c:when test='<%= active.equals("analytics") %>'>
                        <li class="active"><a href="/analytics/">analytics</a></li>
                            </c:when>
                            <c:otherwise>
                        <li><a href="/analytics/">analytics</a></li>
                            </c:otherwise>
                        </c:choose>
                        </c:when>
                    </c:choose>

                    <c:choose>
                        <c:when test='<%= console == true %>'>
                        <c:choose>
                            <c:when test='<%= active.equals("console") %>'>
                        <li class="active"><a><fmt:message key="users"/></a></li>
                            </c:when>
                            <c:otherwise>
                        <li><a href="<%= ldapadm %>/manager/"><fmt:message key="users"/></a></li>
                            </c:otherwise>
                        </c:choose>
                        </c:when>
                    </c:choose>

                </ul>
            </li>
            </c:when>
        </c:choose>
        </ul>

        <div id="lang_switcher">
            <div>
                <a href="<%=switchLang("sk", active, gnService, gnPage)%>">SK</a> |
                <a href="<%=switchLang("en", active, gnService, gnPage)%>">EN</a> |
                <a href="<%=switchLang("fr", active, gnService, gnPage)%>">FR</a>
            </div>
            <div>
                <c:choose>
                    <c:when test='<%= anonymous == false %>'>
                        <p class="logged">
                            <a href="<%=ldapadm %>/account/userdetails"><%=request.getHeader("sec-username") %></a><span class="light"> | </span><a href="/logout"><fmt:message key="logout"/></a>
                        </p>
                    </c:when>
                    <c:otherwise>
                        <p class="logged">
                            <a id="login_a" href=""><fmt:message key="login"/></a>
                        </p>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>

    <script>
        (function(){
            // required to get the correct redirect after login, see https://github.com/georchestra/georchestra/issues/170
            var url,
                a = document.getElementById("login_a"),
                cnxblk = document.querySelector('#go_head p.logged');
            if (a !== null) {
                url = parent.window.location.href;
                if (/\/cas\//.test(url)) {
                    a.href = "/cas/login";
                } else {
                    // removing any existing anchor from URL first:
                    // see https://github.com/georchestra/georchestra/issues/1032
                    var p = url.split('#', 2),
                    /* Taken from https://github.com/openlayers/openlayers/blob/master/lib/OpenLayers/Util.js#L557 */
                    paramStr = "login", parts = (p[0] + " ").split(/[?&]/);
                    a.href = p[0] + (parts.pop() === " " ?
                        paramStr :
                        parts.length ? "&" + paramStr : "?" + paramStr) +
                        // adding potential anchor
                        (p.length == 2 ? "#" + p[1] : "");
                }
            }

            // handle menus
            if (!window.addEventListener || !document.querySelectorAll) return;
            var each = function(els, callback) {
                for (var i = 0, l=els.length ; i<l ; i++) {
                    callback(els[i]);
                }
            }
            each(document.querySelectorAll('#go_head li a'), function(a){
                var li = a.parentNode;
                var ul = li.querySelectorAll('ul');
                a.addEventListener('click', function(e) {
                    each(
                        document.querySelectorAll('#go_head li'),
                        function(l){ l.classList.remove('active');}
                    );
                    if (ul[0]) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        e.preventDefault();
                        li.classList.toggle('expanded');
                        // hide/show connexion block:
                        cnxblk.style.visibility = 
                            cnxblk.style.visibility == '' ? 'hidden' : '';
                    } else {
                        a.parentNode.className = 'active';
                    }
                });
            });
        })();
    </script>

</body>
</html>
